var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var resolution = 256
var arraySize = 11
var initSize = 0.1
var filling = []
var positions
positions = create2DArray(arraySize, arraySize, 0, false)
var sizes = []

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  for (var i = 0; i < arraySize * arraySize; i++) {
    filling.push(generateColorSequence(resolution))
    sizes.push(1 + Math.random() * 3)
  }
  positions = shuffle(positions.flat())
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  background(colors.light)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < arraySize; i++) {
    for (var j = 0; j < arraySize; j++) {
      push()
      translate(windowWidth * 0.5 + (positions[i * arraySize + j][0] - Math.floor(arraySize * 0.5)) * boardSize * initSize + boardSize * initSize * positions[i * arraySize + j][2], windowHeight * 0.5 + (positions[i * arraySize + j][1] - Math.floor(arraySize * 0.5)) * boardSize * initSize + boardSize * initSize * positions[i * arraySize + j][3])
      for (var k = 0; k < resolution; k++) {
        push()
        strokeWeight(1)
        stroke(filling[i * arraySize + j][k] * 255)
        fill(filling[i * arraySize + j][k] * 255)
        rotate(sizes[i * arraySize + j])
        arc(0, 0, boardSize * initSize * sizes[i * arraySize + j], boardSize * initSize * sizes[i * arraySize + j], (k / resolution) * Math.PI * 2, ((1 + k) / resolution) * Math.PI * 2, PIE)
        pop()
      }
      pop()
    }
  }

  filling = []
  sizes = []
  for (var i = 0; i < arraySize * arraySize; i++) {
    filling.push(generateColorSequence(resolution))
    sizes.push(1 + Math.random() * 3)
  }
  positions = shuffle(positions)

  // masking
  fill(colors.light)
  noStroke()
  rect(windowWidth * 0.5, (windowHeight - boardSize) * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect(windowWidth * 0.5, windowHeight * 0.75 + boardSize * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect((windowWidth - boardSize) * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)
  rect(windowWidth * 0.75 + boardSize * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function generateColorSequence(resolution) {
  var finalSequence = []
  var sequence = null
  var xoff = Math.random() * 100
  for (var i = 0; i < resolution; i++) {
    var fillColor = noise(xoff)
    sequence = fillColor
    xoff += 0.05
    finalSequence.push(sequence)
    sequence = null
  }
  return finalSequence
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = [i, j, -0.5 + Math.random(), -0.5 + Math.random()]
      }
    }
    array[i] = columns
  }
  return array
}
